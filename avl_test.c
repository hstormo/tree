#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include "avl.h"


#define depth(n) ((n)? (n)->depth : 0)

void print_tree(avl_node* node)
{
    if (!node) return;
    printf("%.2i -> l=%.2i r=%.2i -- %.2i\n", *(int*)node->data, (node->left)? *(int*)node->left->data : 0, (node->right)? *(int*)node->right->data : 0, node->depth);
    print_tree(node->left);
    print_tree(node->right);
}

int compare_ints(void* l, void* r)
{
    int i1 = *(int*)l, i2 = *(int*)r;
    if (i1 < i2) return -1;
    if (i1 > i2) return  1;
    return 0;
}

void validate_subtree(avl_node* node)
{
    assert(node->depth == max(depth(node->left), depth(node->right)) + 1);
    int balance = depth(node->left) - depth(node->right);
    assert(balance < 2 && balance > -2);

    if (!node->left && !node->right)
    {
        assert(node->depth == 1);
    }

    if (node->left)
    {
        assert(node->left->parent == node);
        validate_subtree(node->left);
    }

    if (node->right)
    {
        assert(node->right->parent == node);
        validate_subtree(node->right);
    }
}

void validate_tree(avl_tree* tree)
{
    if (tree->root)
    {
        assert(tree->root->parent == NULL);
        validate_subtree(tree->root);
    }
}

int main()
{
    int i;

    srand(time(0));

    avl_tree* tree = (avl_tree*) malloc(sizeof(avl_tree));
    avl_init(tree, compare_ints, malloc, free);

    for (i = 0; i < 100; ++i)
    {
        int* d = malloc(sizeof(int));
        *d = (rand());
        avl_insert(tree, d);
        validate_tree(tree);
    }

    avl_remove_all(tree);
    validate_tree(tree);

    printf("Done.\n");

    return 0;
}
