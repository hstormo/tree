#ifndef _AVL_H
#define _AVL_H

#include <stdint.h>

typedef int (*avl_compare_fn)(void*, void*);
typedef void* (*avl_alloc_fn)(size_t);
typedef void (*avl_free_fn)(void*);

typedef struct avl_node avl_node;
typedef struct avl_tree avl_tree;

struct avl_node
{
    void* data;
    avl_node *left, *right, *parent;
    int depth;
};

struct avl_tree
{
    avl_node* root;
    avl_compare_fn comp;
    avl_alloc_fn alloc;
    avl_free_fn free;
};

void avl_init(avl_tree* tree, int (*comp)(void*, void*), void* (*alloc)(size_t), void (*free)(void*));
void avl_remove_all(avl_tree* tree);
avl_node* avl_insert(avl_tree* tree, void* data);
avl_node* avl_find(avl_tree* tree, void* data);
avl_node* avl_inorder_first(avl_tree* tree);
avl_node* avl_inorder_last(avl_tree* tree);
avl_node* avl_inorder_next(avl_node* node);
avl_node* avl_inorder_prev(avl_node* node);

#endif // _AVL_H
