#include <assert.h>
#include "avl.h"

#define depth(n) ((n)? (n)->depth : 0)
#define max(a,b) (((a) < (b))? (b) : (a))

static void set_depth(avl_node* node)
{
    node->depth = max(depth(node->left), depth(node->right)) + 1;
}

static avl_node* left_rotate(avl_node* node)
{
    avl_node* temp = node->right;
    temp->parent = node->parent;
    if (temp->parent)
    {
        if (temp->parent->right == node)
            temp->parent->right = temp;
        else if (temp->parent->left == node)
            temp->parent->left = temp;
    }

    node->right = temp->left;
    if (node->right) node->right->parent = node;

    temp->left = node;
    temp->left->parent = temp;

    set_depth(temp->left);
    set_depth(temp);

    return temp;
}

static avl_node* right_rotate(avl_node* node)
{
    avl_node* temp = node->left;
    temp->parent = node->parent;
    if (temp->parent)
    {
        if (temp->parent->right == node)
            temp->parent->right = temp;
        else if (temp->parent->left == node)
            temp->parent->left = temp;
    }

    node->left = temp->right;
    if (node->left) node->left->parent = node;

    temp->right = node;
    temp->right->parent = temp;

    set_depth(temp->right);
    set_depth(temp);

    return temp;
}

static void free_recursively(avl_free_fn free, avl_node* node)
{
    if (node->left)  free_recursively(free, node->left);
    if (node->right) free_recursively(free, node->right);
    free(node);
}

void avl_init(avl_tree* tree, int (*comp)(void*, void*), void* (*alloc)(size_t), void (*free)(void*))
{
    assert(comp && alloc); // comp and alloc functions are needed for the tree to function, free is optional
    tree->root = NULL;
    tree->comp = comp;
    tree->alloc = alloc;
    tree->free = free ? free : NULL;
}

// Removes all nodes from the tree.
void avl_remove_all(avl_tree* tree)
{
    if (tree->free && tree->root)
        free_recursively(tree->free, tree->root);
    tree->root = NULL;
}

// Insert a new node in the tree. If data is compared to be equal to other nodes already in the tree, the new node is inserted to the right of those nodes, as if it were larger.
avl_node* avl_insert(avl_tree* tree, void* data)
{
    avl_node* new_node = tree->alloc(sizeof(avl_node));
    new_node->data = data;
    new_node->left = new_node->right = new_node->parent = NULL;
    new_node->depth = 1;

    if (!tree->root)
    {
        // Tree is empty -- set new node as root
        tree->root = new_node;
    }
    else
    {
        // Insert the new leaf
        avl_node* parent = tree->root;
        while (parent)
        {
            int c = tree->comp(new_node->data, parent->data);
            if (c < 0)
            {
                if (parent->left == NULL)
                {
                    parent->left = new_node;
                    new_node->parent = parent;
                    break;
                }
                else
                    parent = parent->left;
            }
            else
            {
                if (parent->right == NULL)
                {
                    parent->right = new_node;
                    new_node->parent = parent;
                    break;
                }
                else
                    parent = parent->right;
            }
        }

        // Rebalance ancestors in order
        avl_node* node = parent;
        while (node)
        {
            set_depth(node);
            int diff = depth(node->left) - depth(node->right);
            if (diff > 1)
            {
                // Left child is too heavy
                if (depth(node->left->left) < depth(node->left->right))
                    node->left = left_rotate(node->left);
                node = right_rotate(node);
            }
            else if (diff < -1)
            {
                // Right child is too heavy
                if (depth(node->right->left) > depth(node->right->right))
                    node->right = right_rotate(node->right);
                node = left_rotate(node);
            }
            if (!node->parent)
                tree->root = node;
            node = node->parent;
        }
    }

    return new_node;
}

avl_node* avl_find(avl_tree* tree, void* data)
{
    avl_node* node = tree->root;
    while (node) {
        int c = tree->comp(data, node->data);
        if (c == 0)
            return node;
        else if (c < 0)
            node = node->left;
        else
            node = node->right;
    }
    return NULL;
}

avl_node* avl_inorder_first(avl_tree* tree)
{
    avl_node* node = tree->root;
    if (!node) return NULL;
    while (node->left)
        node = node->left;
    return node;
}

avl_node* avl_inorder_last(avl_tree* tree)
{
    avl_node* node = tree->root;
    if (!node) return NULL;
    while (node->right)
        node = node->right;
    return node;
}

avl_node* avl_inorder_next(avl_node* node)
{
    // If there is a right subtree, return the leftmost node in that subtree
    if (node->right)
    {
        node = node->right;
        while(node->left)
            node = node->left;
        return node;
    }

    // Climb up the tree and find the first link where the parent is to the right of the child. Return that parent.
    if (node->parent)
    {
        while (node->parent && node == node->parent->right)
            node = node->parent;
        return node->parent;
    }

    return NULL;
}

avl_node* avl_inorder_prev(avl_node* node)
{
    // If there is a left subtree, return the rightmost node in that subtree
    if (node->left)
    {
        node = node->left;
        while(node->right)
            node = node->right;
        return node;
    }

    // Climb up the tree and find the first link where the parent is to the left of the child. Return that parent.
    if (node->parent)
    {
        while (node->parent && node == node->parent->left)
            node = node->parent;
        return node->parent;
    }

    return NULL;
}
