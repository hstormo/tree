#ifndef _OST_H
#define _OST_H

#include <stdint.h>

typedef int (*ost_compare_fn)(void*, void*);
typedef void* (*ost_alloc_fn)(size_t);
typedef void (*ost_free_fn)(void*);

typedef struct ost_node ost_node;
typedef struct ost ost;

struct ost_node
{
    void* data;
    ost_node *left, *right, *parent;
    int depth_left, depth_right, weight_left, weight_right;
};

struct ost
{
    ost_node* root;
    ost_compare_fn comp;
    ost_alloc_fn alloc;
    ost_free_fn free;
};

void ost_init(ost* tree, int (*comp)(void*, void*), void* (*alloc)(size_t), void (*free)(void*));
void ost_remove_all(ost* tree);
ost_node* ost_insert(ost* tree, void* data);
ost_node* ost_find(ost* tree, void* data);
int ost_count(ost* tree);
ost_node* ost_inorder_first(ost* tree);
ost_node* ost_inorder_last(ost* tree);
ost_node* ost_inorder_next(ost_node* node);
ost_node* ost_inorder_prev(ost_node* node);
ost_node* ost_select(ost* tree, int index);
int ost_index_of(ost_node* node);
int ost_rank_of(ost* tree, ost_node* node);

#endif // _OST_H
