#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include "ost.h"

#define depth(n) ((n)? max((n)->depth_left, (n)->depth_right) + 1 : 0)
#define weight(n) ((n)? (n)->weight_left + (n)->weight_right + 1 : 0)

void print_tree(ost_node* node)
{
    if (!node) return;
    printf("%.2i -> l=%.2i r=%.2i -- %.2i / %.4i\n", *(int*)node->data, (node->left)? *(int*)node->left->data : 0, (node->right)? *(int*)node->right->data : 0, depth(node), weight(node));
    print_tree(node->left);
    print_tree(node->right);
}

int compare_ints(void* l, void* r)
{
    int i1 = *(int*)l, i2 = *(int*)r;
    if (i1 < i2) return -1;
    if (i1 > i2) return  1;
    return 0;
}

void validate_subtree(ost_node* node)
{
    int balance = depth(node->left) - depth(node->right);
    assert(balance < 2 && balance > -2);
    assert(weight(node) == weight(node->left) + weight(node->right) + 1);
    assert(node->depth_left == depth(node->left));
    assert(node->depth_right == depth(node->right));
    assert(node->weight_left == weight(node->left));
    assert(node->weight_right == weight(node->right));

    if (!node->left && !node->right)
    {
        assert(depth(node) == 1);
    }

    if (node->left)
    {
        assert(node->left->parent == node);
        validate_subtree(node->left);
    }

    if (node->right)
    {
        assert(node->right->parent == node);
        validate_subtree(node->right);
    }
}

void validate_tree(ost* tree)
{
    if (tree->root)
    {
        assert(tree->root->parent == NULL);
        validate_subtree(tree->root);
    }
}

int main()
{
    int i;

    srand(time(0));

    ost* tree = (ost*) malloc(sizeof(ost));
    ost_init(tree, compare_ints, malloc, free);

    for (i = 0; i < 1000; ++i)
    {
        int* d = malloc(sizeof(int));
        *d = ((rand()%100));
        ost_insert(tree, d);
        validate_tree(tree);
    }

    ost_node* node = ost_inorder_first(tree);
    for (i = 0; i < 1000; ++i)
    {
        assert(node);
        assert(node == ost_select(tree, i));
        assert(ost_index_of(node) == i);
        printf("%.5i  ", *(int*)node->data);
        node = ost_inorder_next(node);
    }

    ost_remove_all(tree);
    validate_tree(tree);

    printf("Done.\n");

    return 0;
}
